# mihira.me

Code for mihira.me website

## Usage

Build
```bash
docker-compose build
```

Local run
```bash
docker-compose up mihira.me.localhost
```

It will be available at `http://localhost/`