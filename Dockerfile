FROM nginxinc/nginx-unprivileged:1.25

ENV TZ=Europe/Berlin

COPY html /usr/share/nginx/html/
